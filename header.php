<!---Home Bar--->
<nav id="navbar" class="custom-navbar">
  <div class="nav-wrapper">

    <a id="logo" href="index.php?page=home" class="brand-logo"><img src="assets/images/logo.png" /></a>

    <ul id="nav-mobile" class="right hide-on-med-and-down">
      <li><a href="index.php?page=home">Home</a></li>
      <li><a href="index.php?page=about">About</a></li>
    </ul>

  </div>
</nav>

<!---Result Bar--->
<div class="navbar-fixed">
  <nav id="navbar_hidden" class="custom-navbar" style="display:none">
    <div class="nav-wrapper">

      <a id="logo" href="index.php?page=home">Tell Me About: <span id="searched" class="text-highlight"></span> </a>

      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="index.php?page=home">Home</a></li>
      </ul>

    </div>
  </nav>
</div>
