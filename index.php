<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <title>TellMe</title>

    <!---External--->
    <script src="lib/jquery-3.1.1.min.js"></script>
    <script src="lib/materialize.min.js"></script>
    <link rel="stylesheet" href="lib/materialize.min.css">
    <link rel="stylesheet" href="lib/animate.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|PT+Sans">

    <!---Custom Scripts--->
    <script src="assets/js/functions.js"></script>
    <script src="assets/js/pageload.js"></script>

    <!---Stylesheets--->
    <link rel="stylesheet" href="assets/css/global.css">

    <?php include("assets/php/functions.php"); ?>
    <?php include("header.php"); ?>
  </head>
  <body>
    <div id="div_main" style="margin: 0 4% 0 8%">
      <?php
        if (isset($_GET["page"])) {
          $page = $_GET['page'];
        }
        if (!empty($page)) {
          $location = "pages/".$page.".php";
          include($location);
        }
        else {
          include("pages/home.php");
        }
      ?>
    </div>
  </body>
    <?php include("footer.php"); ?>
</html>
