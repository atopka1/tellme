<script>
  $(document).ready(function() {
    var search = getQueryVariable("search");
    // var container = "#statements";

    getStatements(search, "#verified_statements", "verified");
    getStatements(search, "#unverified_statements", "unverified");
    animateSearch();

    //update result bar
    $("#searched").text(search);

  });
</script>

<div id="result_wrapper" class="center-align" style="display:none">

  <div class="row">
    <img class="sfireImage animated fadeIn" src="assets/images/example-2.jpg" />
  </div>

  <div class="row" style="margin-bottom: 0">

    <div class="col s6 verified" style="margin-left: -2%">
      <p class="animated bounceInDown"><img src="assets/images/checkmark.png">Verified</p>
    </div>

    <div class="col s6 unverified" style="margin-left: -2.5%">
      <p class="animated bounceInDown"><img src="assets/images/xmark.png"> Unverified</p>
    </div>

  </div>
  <div class="row" style="margin-left: 5%">
    <!--Verified-->
    <div class="col s6">
      <div class="row left-align">
        <ul id="verified_statements" class="sfireList statement-list">

        </ul>
      </div>
    </div>

    <!--Unverified-->
    <div class="col s6">
      <div class="row left-align">
        <ul id="unverified_statements" class="sfireList statement-list">

        </ul>
      </div>
    </div>

  </div>

  <!-- <div class="row verified">
    <p><img src="assets/images/checkmark.png">Verified</p>
  </div>

  <div id="list_bias" class="row left-align">
    <ul id="verified_statements" class="sfireList statement-list">

    </ul>
  </div>

  <div class="row">
    <ul id="unverified_statements" class="sfireList statement-list">
    </ul>
  </div>

  <div class="row unverified">
    <p><img src="assets/images/xmark.png"> Unverified</p>
  </div> -->

</div>
