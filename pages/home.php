<div id="home_hidden_bar" style="display:none">
    <p>Tell Me About: <span id="searched" class="text-highlight" style="text-transform: capitalize"></span></p>
</div>


<div id="home_search_bar" class="well">

    <p class="top-label">Tell Me About: <span id="trending_topic" class="text-highlight" style="text-transform: capitalize"></span></p>

    <div id="home_form_wrapper">
      <form id="search_form">
        <input id="search_input" type="text" name="search_term" style="color:black;" placeholder="hint: type 'uber', 'waymo', or 'LIDAR' for the demo."></input>
        <button id="search_submit">Go</button>
      </form>
    </div>

</div>

<div id="home_results" style="display:none;" class="centered" style="margin-top: -200px"></div>
