<?php

  require("functions.php");

  if (isset($_GET["search"])) {
    $search = $_GET["search"];
    $type = $_GET["type"];
    getStatementsFromFile($search, $type);
  }
  else {
    die("Query not set.");
  }

  //demo function, obtains data from example.json
  function getStatementsFromFile($search, $type) {
      $search = strtolower($search);
      $search = preg_replace('/\s+/', ' ', $search);

      $string = file_get_contents("example.json");
      $json = json_decode($string, true);

      if ($type == "verified") {
        showStatements($json["verified"], $search, "text-bubble");
      }
      else if ($type == "unverified") {
        showStatements($json["unverified"], $search, "text-bubble alt-bubble");
      }
      else {
        die("Information not found. Check dataHandler.php");
      }
} //getFromJSONFile

function showStatements($array, $search, $css_class) {

    foreach($array as $statement) {
        //if search matches a keyword
        if (in_array($search, $statement["keywords"])) {

            //open wrapper
            echo "<div class='col s12' style='padding: 0;'>";

            //show statement
            echo
                "<div class='col s9'>"
                ."<li class='".$css_class." animated slideInDown'><span>"
                .$statement["text"]
                ."</span></li>"
                ."</div>";

            //show images
            echo "<div class='col s3' style='padding: 0'><li class='list-logo'>";
            foreach($statement["sources"] as $source) {
              $image = "<img src='assets/images/".$source["source"].".png' class='source-logo animated fadeIn' />";
              echo
                  "<a href='".$source["link"]."'>".$image."</a>";
            }
            echo "</li></div>";

            //close wrapper
            echo
                  "</div>";
        }

    } //end loop
}

?>
