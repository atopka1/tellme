//placeholder for trending topic animation
function demoTrend() {

    $.ajax({
      url: "/assets/js/trending.json",
      dataType: "json",
      success: function(data) {

        var trends = data["trends"];
        var trend;
        setTimeout(function() {
          trend = trends[Math.floor((Math.random() * trends.length-1) + 0)];
          $("#trending_topic").html(trend);
        }, 3000);
      },
      error: function() {

      }
    });
}

function animateSearch() {
    $("#navbar").slideUp(800, function() {
      $("#navbar_hidden").slideDown(800, function() {
        $("#result_wrapper").fadeIn(800);
        Materialize.showStaggeredList('#statements');
      });
    });
}

function showHiddenNav() {
    $("#navbar_hidden").slideDown();
}

//retrives demo summaries from json
function demoSummary() {

}

function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
           var pair = vars[i].split("=");
           if(pair[0] == variable){return pair[1];}
   }
   return(false);
}

function showNotFound() {
    window.location = "pages/notfound.php";
} //end showNotFound

//constructs a string for the HTML element with applied attributes
function constructElement(eElement, eId, eClass, eStyle, eContent) {
    eId = eId || "";
    eClass = eClass || "";
    eStyle = eStyle || "";
    eContent = eContent || null;

    var element = ["<", eElement, " id='", eId, "' class='", eClass, "' style='", eStyle, "'>", eContent, "</", eElement, ">"];
    return element.join('');
}

function getSummaries() {
    var container = "#result_content";
    $.ajax({
      url: "assets/js/demo.json",
      type: "GET",
      dataType: "json",
      success: function(data) {
        for (i=0; i<data; i++) {
          constructSummary(data["summaries"][i], container);
        }
      },
      error: function() {

      }
    }); //end ajax
} //end getSummaries

function constructSummary(data, container) {
    var arr = [];

    //Image
    arr.push("<div class='row'>");
    arr.push("<img class='sfireImage' src='"+data["image"]+"' />");
    arr.push("</div>");

    //Bias
    arr.push("<div class='row'>");

    arr.push("</div>");
}

function getStatements(search, container, type) {
    $.ajax({
        url: "assets/php/dataHandler.php",
        type: "GET",
        dataType: "text",
        data: {
          "search" : search,
          "type" : type
        },
        success: function(html) {
          $(container).append(html);
        },
        error: function() {
          console.log("Something went wrong accessing php file.");
        }
    });
} //getStatements
