$(document).ready(function() {

   // ScrollFire //
   var options = [
        {selector: ".sfireList", offset: 200, callback: function() {
          Materialize.showStaggeredList('#verified_statements');
          Materialize.showStaggeredList('#unverified_statements');
        }},
        {selector: ".sfireImage", offset: 200, callback: function() {
          Materialize.fadeInImage('.sfireImage');
        }},
        {selector: ".sfireFade", offset: 200, callback: function() {
          $(this).fadeIn();
        }},
    ];
    Materialize.scrollFire(options);


    //Trending Topics (currently hardcoded)
    // demoTrend();
    //
    // var delay =  3000;
    // window.setInterval(function() {
    //     demoTrend();
    // }, delay);

    //Click current trending topic shown
    // $("#trending_topic").click(function() {
    //     window.clearInterval();
    //     var topic = $(this).html();
    //     $("#search_input").html(topic);
    //     animateSearchBar();
    // });

    //Click 'Go'
    $("#search_submit").click(function(e) {
        e.preventDefault();

        var inputContainer = "#search_input";
        var searchBar = "#home_search_bar";

        var search = $(inputContainer).val();
        $(searchBar).fadeOut(500, function() {
          window.location = "index.php?page=result&search="+search;
        });
    }); //end search_submit click
}); //end ready
