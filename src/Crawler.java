import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;
import java.net.*;
import org.jsoup.Jsoup;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Crawler {
  private static final int MAX_PAGES_TO_SEARCH = 100;
  private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
  private Set<String> pagesVisited = new HashSet<String>();
  private List<String> pagesToVisit = new LinkedList<String>();
  public List<String> urlsToReturn = new LinkedList<String>();
  private String domain = "";

  public void initCrawler(String url, String keyword) {
    this.domain = getDomain(url);
    while(this.pagesVisited.size() < MAX_PAGES_TO_SEARCH) {
      long start = System.nanoTime();
      String currentUrl;
      CrawlerLeg leg = new CrawlerLeg();
      if (this.pagesToVisit.isEmpty()) {
        currentUrl = url;
        this.pagesVisited.add(url);
      }
      else {
        currentUrl = this.nextUrl();
      }
      leg.crawl(currentUrl);

      //Check if keyword was found. If so, add to urls to return.
      boolean success = leg.searchForWord(keyword);
      if (success) {
        System.out.println("***Word Found*** " + keyword + " was found at " + currentUrl);
        urlsToReturn.add(currentUrl);
      }
      this.pagesToVisit.addAll(leg.getLinks());
      System.out.println("Pages searched: " + pagesVisited.size());
      long elapsedTime = System.nanoTime() - start;
      double seconds = (double) elapsedTime/1000000000.0;
      System.out.println("Total Time: " + seconds);
    } //end while

    System.out.println("\n ***DONE*** " + urlsToReturn.size() + " urls found.");
    printUrls();
  } //end initCrawler

  public void printUrls() {
    //print urlsToReturn
    for (int i=0; i<urlsToReturn.size(); i++) {
      System.out.println(this.urlsToReturn.get(i));
    }
  } //end printUrls

  public String nextUrl() {
    String nextUrl;
    do {
      nextUrl = this.pagesToVisit.remove(0);
    } while (this.pagesVisited.contains(nextUrl) || !validateUrl(nextUrl));
    this.pagesVisited.add(nextUrl);
    return nextUrl;
  }

  public boolean validateUrl(String url) {
    if (url == "" || !url.contains(this.domain)) {
      return false;
    }
    else {
      return true;
    }
  } //end validateUrl

// public boolean validateUrl(String url) {
//   String domain = "";
//   try {
//     domain = new URI(this.htmlDocument.location()).getHost();
//   } catch (Exception e) {
//     e.printStackTrace();
//   }
//   //FILTER HERE
//   if (url.contains(domain)) {
//     return true;
//   }
//   else {
//     return false;
//   }
// } //end validateUrl

  public String getDomain(String url) {
    String _domain = "";
    try {
      Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
      Document htmlDocument = connection.get();
      try {
        _domain = new URI(htmlDocument.location()).getHost();
      } catch (Exception e) {
        e.printStackTrace();
      }
    } catch(Exception ioe) {

    }
    return _domain;
  } //end getDomain
} //end crawler class
