import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.net.*;

import org.jsoup.Jsoup;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawlerLeg {
  private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
  private List<String> links = new LinkedList<String>();
  private Document htmlDocument;

  public boolean crawl(String url) {
    long start = System.nanoTime();
    try {
      Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
      Document htmlDocument = connection.get();
      this.htmlDocument = htmlDocument;
      if (connection.response().statusCode() == 200) {
        System.out.println("\n**Visiting** Received web page at " + url);
      }
      if (!connection.response().contentType().contains("text/html")) {
        System.out.println("**Failure** Retrieved something other than HTML");
        return false;
      }
      Elements linksOnPage = htmlDocument.select("a[href]");
      for (Element link : linksOnPage) {
        this.links.add(link.absUrl("href"));
      }
      long elapsedTime = System.nanoTime() - start;
      double seconds = (double) elapsedTime/1000000000.0;
      System.out.println("Crawl Time: " + seconds);
      return true;
    } catch(IOException ioe) {
      return false;
    }
  } //end crawl

  //Search in the body of the HTML document retrieved. Call only after crawl has completed and returned successfully.
  public boolean searchForWord(String searchWord) {
    //check for empty document
    if (this.htmlDocument == null) {
      System.out.println("ERROR: Call crawl() before checking body on this document");
      return false;
    }
    String bodyText = this.htmlDocument.body().text();
    return bodyText.toLowerCase().contains(searchWord.toLowerCase());
  } //end searchForWord

  public List<String> getLinks() {
    return this.links;
  } //end getLinks
}
