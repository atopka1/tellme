import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Iterator;

public class Crawler {
  private static final int MAX_PAGES_TO_SEARCH = 100;
  private Set<String> pagesVisited = new HashSet<String>();
  private List<String> pagesToVisit = new LinkedList<String>();

  public void initCrawler(String url, String keyword) {
    while(this.pagesVisited.size() < MAX_PAGES_TO_SEARCH) {
      String currentUrl;
      CrawlerLeg leg = new CrawlerLeg();
      if (this.pagesToVisit.isEmpty()) {
        currentUrl = url;
        this.pagesVisited.add(url);
      }
      else {
        currentUrl = this.nextUrl();
        //currentUrl = null;
      }
      leg.crawl(currentUrl);
      boolean success = leg.searchForWord(keyword);
      if (success) {
        System.out.println(String.format("**Success** Word %s found at %s", keyword, currentUrl));
        break;
      }
      this.pagesToVisit.addAll(leg.getLinks());
    } //end while
    //print out all pages Visited
    Iterator<String> it = pagesVisited.iterator();
    System.out.println("******Pages Visited*****");
    while(it.hasNext()) {
      System.out.println(it.next());
    };
    System.out.println("\n**Done** Visited " + this.pagesVisited.size() + " web page(s)");
  } //end initCrawler

  private String nextUrl() {
    String nextUrl;
    do {
      nextUrl = this.pagesToVisit.remove(0);
    } while (this.pagesVisited.contains(nextUrl));
    this.pagesVisited.add(nextUrl);
    return nextUrl;
  } //end nextUrl
} //end Crawler class
