public class CrawlerTest {
  public static void main(String[] args) {
    String[] urls = {"http://foxnews.com/", "http://nytimes.com/", "http://www.washingtonpost.com", "http://www.cnn.com"};
    String url = "http://nytimes.com";
    String keyword = "Donald Trump";

    Crawler crawler = new Crawler();
    crawler.initCrawler(url, keyword);
  }
}
