import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;

public class Crawler {
  private static final int MAX_PAGES_TO_SEARCH = 20;
  // private ArrayList<String> pagesToVisit = new ArrayList<String>();
  // private ArrayList<String> pagesVisited = new ArrayList<String>();
  public ArrayList<String> urlsToReturn = new ArrayList<String>();
  private Set<String> pagesVisited = new HashSet<String>();
  public Set<String> pagesToVisit = new HashSet<String>();


  public void initCrawler(String url, String keyword) {
    //queue urls for visit
    // addUrls(urls, this.pagesToVisit);
    pagesToVisit.add(url);
    int pagesRemaining;
    boolean done = false;
    String currentUrl;

    System.out.println("\n<------- STARTING CRAWLER ------>\n");

    //crawl each url, adding links that match the keyword
    while (this.pagesVisited.size() < MAX_PAGES_TO_SEARCH && !this.pagesToVisit.isEmpty() && !done) {
      pagesRemaining = this.pagesToVisit.size();
      System.out.println("Pages remaining: " + pagesRemaining);
      System.out.println("Pages traversed: " + this.pagesVisited.size());

      //Get current url
      currentUrl = this.pagesToVisit.get(pagesRemaining - 1);

      //Crawl the page
      CrawlerLeg leg = new CrawlerLeg();
      boolean success = leg.crawl(currentUrl);

      //Add Links to pages to visit
      if (success) {
        leg.addLinks(pagesToVisit);
      }

      //Look for keyword. If found, add to return list
      if (leg.checkBody(keyword)) {
        urlsToReturn.add(currentUrl);
      }


      // //Check if page already visited
      // if (currentUrl == "" || this.pagesVisited.contains(currentUrl)) {
      //   System.out.println("\n***ERROR*** empty or repeat url. Url is " + currentUrl);
      //   this.pagesToVisit.remove(pagesRemaining-1);
      // }
      // else {
      //   //Crawl the page
      //   CrawlerLeg leg = new CrawlerLeg();
      //   boolean success = leg.crawl(currentUrl);
      //
      //   //Remove this page and add it to pagesVisited
      //   this.pagesVisited.add(currentUrl);
      //   this.pagesToVisit.remove(pagesRemaining - 1);
      //
      //   //Add links to pages to visit
      //   if (success) {
      //     this.pagesToVisit.addAll(leg.getLinks(urls));
      //     //Add this url to list if it contains the keyword in the body
      //     if (leg.checkBody(keyword)) {
      //       System.out.print(keyword + " is in " + leg.htmlDocument.location() + "...adding to list.");
      //       urlsToReturn.add(currentUrl);
      //     }
      //   }
      // }
    } //end while

    System.out.println("\n<------- CRAWLER FINISHED ------>\n");
    System.out.println("Links Found: " + this.urlsToReturn.size());
    printList(this.urlsToReturn);
  } //end initCrawler

  //adds urls to list to visit
  public void addUrls(String[] urls, ArrayList<String> list) {
    for (int i=0; i<urls.length; i++) {
      list.add(urls[i]);
    }
  } //end addUrls

  //DEBUG: show urls that were obtained
  public void printList(ArrayList<String> list) {
    for (int i=0; i<list.size(); i++) {
      System.out.println(list.get(i));
    }
  } //end printList
}
