import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.net.*;

import org.jsoup.Jsoup;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawlerLeg {
  private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chome/13.0.782.112 Safari/535.1";
  public Document htmlDocument;

  //takes document and checks that it can be crawled
  public boolean crawl(String url) {
    System.out.println("Current url: " + url);
    try {

      //get document
      Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
      Document htmlDocument = connection.get();
      this.htmlDocument = htmlDocument;

      //check connection
      if (connection.response().statusCode() == 200) {
        System.out.println("\n**Visiting** Page at " + url);
        return true;
      }
      //check html found
      if (!connection.response().contentType().contains("text/html")) {
        System.out.println("\n***ERROR*** Retrieved something other than html");
        return false;
      }
      return true;
    } catch (IOException ioe) {
      System.out.println("\n***ERROR*** IO Exception");
      return false;
    }
  } //end crawl

  //return links on page that contain domain from original list
  public ArrayList<String> getLinks(String[] urls) {
    if (this.htmlDocument == null) {
      System.out.println("\n***ERROR*** Call crawl() before checking this document");
      return null;
    }
    ArrayList<String> links = new ArrayList<String>();
    Elements linksOnPage = htmlDocument.select("a[href]");
    String currentLink;

    //get current domain
    // String domain = getDomainName(this.htmlDocument.location());
    String domain = "";
    try {
      domain = new URI(this.htmlDocument.location()).getHost();
    } catch (Exception e) {
      e.printStackTrace();
    }
    //validate each link and add them to return list
    for (Element link : linksOnPage) {
      currentLink = link.absUrl("href");
      // if (validateLink(currentLink, domain)) {
      //   links.add(link.absUrl("href"));
      // }
      if (currentLink.contains(domain) && !currentLink.contains("%")) {
        links.add(currentLink);
      }
    }
    return links;
  } //end getLinks

  public boolean validateLink(String link, String domain) {
    //check for weird characters
    if (link.contains("%") || link.contains("@")) {
      return false;
    }

    //check that link is in one of the domains
    if (!link.contains(domain)) {
      System.out.println(link + " not in domain " + domain);
      return false;
    };

    return true;
  }

  //return ALL links on page
  public ArrayList<String> getAllLinks() {
    ArrayList<String> links = new ArrayList<String>();
    if (this.htmlDocument == null) {
      System.out.println("\n***ERROR*** Call crawl() before checking this document");
      return links;
    }
    Elements linksOnPage = htmlDocument.select("a[href]");
    for (Element link : linksOnPage) {
      String currentLink = link.absUrl("href").toString();
      //check for WEIRD characters
      if (!currentLink.contains("%") && !currentLink.contains("mailto:")) {
        //System.out.println(link.absUrl("href").toString());
        links.add(link.absUrl("href").toString());
      }
    }
    return links;
  } //end getAlLLinks

  //gets links and directly adds them to pagesToVisit
  public void addLinks(Set<String> pagesToVisit) {
    if (this.htmlDocument == null) {
      System.out.println("\n ***ERROR*** Call crawl() before checking this document");
      return null;
    }
    Elements linksOnPage = htmlDocument.select("a[href]");
    String currentLink;

    //get current domain
    String domain = "";
    try {
      domain = new URI(this.htmlDocument.location()).getHost();
    } catch (Exception e) {
      e.printStackTrace();
    }

    //validate each link and add them to return list
    for (Element link : linksOnPage) {
      currentLink = link.absUrl("href");
      if (currentLink.contains(domain) && !currentLink.contains("%")) {
        pagesToVisit.add(currentLink);
      }
    }
  } //end addLinks

  //returns true if the keyword can be found in the body of the document
  public boolean checkBody(String keyword) {
    if (this.htmlDocument == null) {
      System.out.println("\n***ERROR*** Call crawl() before checking this document");
      return false;
    }
    String bodyText = this.htmlDocument.body().text();
    return bodyText.toLowerCase().contains(keyword.toLowerCase());
  } //end checkBody

  //counts how many times the keyword appears in the body of the page
  public int countOccurrence(String keyword) {
    return 0;
  } //end countOccurrence

  public String getDomainName(String url) throws URISyntaxException {
    URI uri = new URI(url);
    String domain = uri.getHost();
    return domain.startsWith("www.") ? domain.substring(4) : domain;
  } //end getDomainName
}
